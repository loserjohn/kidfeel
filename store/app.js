 
import http from '../common/http/index.js'
import SET from '@/SET.js';

const modules = {
	name: 'app',
	namespaced: true,
	state: {
		token: null,
		currentAction: false,
		hasLogin: false,
		inBunding: false, //是否回调绑定页
		userInfo: null, //用户得基本信息
		accountInfo: {}, //账户基本信息
		location: null //用户当前位置
	},
	getters: {
		hasLogin: state => {
			return state.hasLogin
		}
	},
	mutations: {
		// payload为用户传递的值，可以是单一值或者对象
		userLogin(state, payload) {
			state.hasLogin = true;
		},
		userLoginOut(state, payload) {
			uni.removeStorageSync(SET.tokenName)
			uni.removeStorageSync(SET.opIdName)
			uni.removeStorageSync('inviCode')
			state.hasLogin = false;
			state.userInfo = null;
		},
		setToken(state, payload) {
			// debugger
			state.token = payload;
		},
		currentAction(state, payload) {

			state.currentAction = {
				...payload
			};
		},
		inBunding(state, payload) {
			state.inBunding = payload;
		},
		setUserInfo(state, payload) {
			state.userInfo = {
				...payload
			};
		},
		 
	},
	actions: { 
		 
		async refreshUser({
			state,
			commit
		}, payload) {
			// console.log(payload);
			try{
				let res = await http.Auth.getConsumer();
				commit('setUserInfo', res)
			}catch(e){
				//TODO handle the exception
			}
 
		}
	}
}

export default modules
