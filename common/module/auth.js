// 用户身份相关接口
// 我要积分
import http from '../http/interface'
const Auth = {


	// 修改支付宝账号
	BindAliAccount: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Consumer/BindAliAccount',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 修改支付宝账号
	BindAliAccount: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Consumer/BindAliAccount',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 需改绑定手机号
	BindConsumerMobile: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Lease/CarMyManage/ReplaceMobile',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 上传图片
	UploadWebP: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Upload/UploadWebP',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 完善个人信息
	/* 
	"consumer_nick_name": "",//昵称
  "consumer_head": "",//
  头像 */
	PerfectInfo: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Lease/CarMyManage/SetUserInfo',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},


	//code直接判断 账户有效性
	LoginToken: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/LoginToken',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},

	//Openid查询关注
	GetWxOpenid_Attention: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/GetWxOpenid_Attention',
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	},
	// code查询关注
	GetWxcode_Attention: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/GetWxcode_Attention',
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	},
	// code获取OpenId
	GetOpenId: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/GetOpenId',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 微信自动注册
	WxAutoRegiste: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/Registe',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},


	// 获取验证码
	getVerificateCode: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Msg/Note/send',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 微信api
	GetWxJsApiConfig: (data) => {
		return http.request({
			url: '/api/Auth/GetJsApiConfig',
			method: 'POST',
			data,
			// handle:true
		})
	},
	// 登录
	userLogin: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/Token',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 快捷登录
	shortcutToken: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/ShortcutToken',
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	},
	// OpenId登录
	WxTokenLogin: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/WxToken',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 注册用户 
	userRegiste: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/Registe',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 忘记密码
	findPassword: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/FindPassword',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 获取用户信息
	/* @user_id   */
	getConsumer: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Lease/CarMyManage/GetInfo',
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	},

	//更新用户信息
	PerfectPayInfo: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Consumer/PerfectPayInfo',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 设置支付密码
	SettingPayPassword: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Consumer/SettingPayPassword',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	// 微信绑定手机号
	BindWxUserMobile: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/BindWxUserMobile',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},

	// 支付
	toPayment: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Lease/CarPay/Payment',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},

	// 提现
	SubmitCash: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Cash/SubmitCash',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},

	//=============== 全局=================

	// 获取全局配置
	getConfig: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Service/Config',
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	},

	// app更新
	VersionCheck: (data, ifLoad = false) => {
		return http.request({
			url: '/api/AppVersion/VersionCheck',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},

	// 手机号登录
	MobileToken: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Auth/MobileToken',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	
	// 身份认证
	CarMyManage: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Lease/CarMyManage/Atte',
			method: 'POST',
			data,
			// handle:true
		}, ifLoad)
	},
	
	
	// 验证是都是用户首单
	IsFirstOrder: (data, ifLoad = false) => {
		return http.request({
			url: '/api/Lease/CarOrder/IsFirstOrder',
			method: 'GET',
			data,
			// handle:true
		}, ifLoad)
	},
}
export default Auth
