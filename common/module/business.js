// 用户身份相关接口

import http from '../http/interface'
const Set = {

	// 首页接口
	GetHome: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/CarHome/GetHome',
			method: 'GET',
			data,
			// handle:true 
		})
	},
	// 获取推荐网点列表
	GetServiceSite: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/CarHome/GetServiceSite',
			method: 'POST',
			data,
			// handle:true 
		})
	},
	//网点详情
	GetSiteDetail: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/Site/GetSite',
			method: 'GET',
			data,
			// handle:true 
		})
	},
	
	
	//获取所有城市列表
	GetArea: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/CarHome/GetArea',
			method: 'POST',
			data,
			// handle:true 
		})
	},

	// 获取车源列表
	GetCarList: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/CarInfo/GetCarList',
			method: 'POST',
			data,
			// handle:true 
		})
	},

	// 获取车源详情
	GetCar: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/CarInfo/GetCar',
			method: 'GET',
			data,
			// handle:true 
		})
	},

	// 发票管理
	GetTaxOrderList: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/Invoice/GetTaxOrderList',
			method: 'POST',
			data,
			// handle:true 
		})
	},

	// poi检索  page location keywords
	GetPoiList: (data) => {
		let d = {
			page: 1,
			key: '60361faf3cfac69914dbd743ef772c04',
			...data
		}
		return http.request({
			baseUrl: 'https://restapi.amap.com',
			url: '/v3/place/around',
			method: 'GET',
			data: d,
			// handle:true 
		})
	},

	
	// *******************地址************
	// 地址管理
	GetAddressList: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/Address/GetAreaList',
			method: 'POST',
			data,
			// handle:true 
		})
	},
	// 新建地址
	AddAddressList: (data) => {
		return http.request({
			url: '/api/Lease/Address/BindArea',
			method: 'POST',
			data,
			// handle:true
		})
	},
	// 默认地址

	DefaultAddress: (data) => {
		return http.request({
			url: '/api/Lease/Address/DefaultArea',
			method: 'POST',
			data,
			// handle:true
		})
	},
	// 删除地址

	DelAddress: (data) => {
		return http.request({
			url: '/api/Lease/Address/RelieveArea',
			method: 'GET',
			data,
			// handle:true
		})
	},
	// 买车列表
	GetSellCar: (data) => {
		return http.request({
			url: '/api/Lease/SellCar/GetCarList',
			method: 'POST',
			data,
			// handle:true
		})
	},




	// 订单部分
	// 创建订单
	CreateOrder: (data) => {
		return http.request({
			url: '/api/Lease/CarOrder/CreateOrder',
			method: 'POST',
			data,
			// handle:true
		})
	},
	// 订单列表
	GetOrderList: (data) => {
		return http.request({
			url: '/api/Lease/CarOrder/GetOrderList',
			method: 'POST',
			data,
			// handle:true
		})
	},
	
	// 申请退款
	
	OrderRefund: (data) => {
		return http.request({
			url: '/api/Lease/CarRefund/Refund',
			method: 'POST',
			data,
			// handle:true
		})
	},
	
	//一键取车
	AutoGetCar: (data) => {
		return http.request({
			url: '/api/Lease/CarOrder/AutoGetCar',
			method: 'POST',
			data,
			// handle:true
		})
	},
	// 一键还车
	AutoStillCar: (data) => {
		return http.request({
			url: '/api/Lease/CarOrder/AutoStillCar',
			method: 'POST',
			data,
			// handle:true
		})
	},
	// 订单详情
	GetOrderDetail: (data) => {
		return http.request({
			url: '/api/Lease/CarOrder/GetOrderDetail',
			method: 'GET',
			data,
			// handle:true
		})
	},
	
	// 取车还车验车单
	GetCheckDetails: (data) => {
		return http.request({
			url: '/api/Lease/CarCheck/GetCheckDetails',
			method: 'POST',
			data,
			// handle:true
		})
	},
	
	
	
	// *******************二手车部分************
	// 美容列表
	GetBeautySite: (data) => {
		return http.request({
			url: '/api/Lease/CarBeauty/GetBeautySite',
			method: 'POST',
			data,
			// handle:true
		})
	},
	
	// 美容详情
	GetBeautyCarDetail: (data) => {
		return http.request({
			url: '/api/Lease/CarBeauty/GetSite',
			method: 'GET',
			data,
			// handle:true
		})
	},
	
	// 车源详情
	GetSellCarDetail: (data) => {
		return http.request({
			url: '/api/Lease/SellCar/GetCar',
			method: 'GET',
			data,
			// handle:true
		})
	},
		
	// *******************承租人************
	// 承租人信息 
	GetLesseeList: (data) => {
		return http.request({
			// baseUrl:'https://geo.datav.aliyun.com',
			url: '/api/Lease/CarLessee/GetLesseeList',
			method: 'POST',
			data,
			// handle:true 
		})
	},
	// 新建承租人
	BindLessee: (data) => {
		return http.request({
			url: '/api/Lease/CarLessee/BindLessee',
			method: 'POST',
			data,
			// handle:true
		})
	},
 
	// 删除承租人	
	RelieveLessee: (data) => {
		return http.request({
			url: '/api/Lease/CarLessee/RelieveLessee',
			method: 'GET',
			data,
			// handle:true
		})
	}, 
	
	
	// *******************违章罚款************
	 
	GetRulesList: (data) => {
		return http.request({
			url: '/api/Lease/CarRules/GetRulesList',
			method: 'POST',
			data,
			// handle:true
		})
	}, 
	// 违章详情
	GetRulesDetail: (data) => {
		return http.request({
			url: '/api/Lease/CarRules/GetRules',
			method: 'GET',
			data,
			// handle:true
		})
	},
	// 违章处理·
	DealRules: (data) => {
		return http.request({
			url: '/api/Lease/CarRules/DealRules',
			method: 'POST',
			data,
			// handle:true
		})
	},
}
export default Set
