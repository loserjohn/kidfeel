/**
 * 通用uni-app网络请求
 * 基于 Promise 对象实现更简单的 request 使用方式，支持请求和响应拦截
 */
import store from '../../store/index.js'
import SET from '../../SET.js'
export default {
	config: { 
		baseUrl: SET.baseUrl, 
		header: {
			'Content-Type': 'application/json;charset=UTF-8', 
		},
		data: {},
		method: "GET",
		dataType: "json",
		/* 如设为json，会对返回的数据做一次 JSON.parse */
		responseType: "text",
		success() {},
		fail() {},
		complete() {
			
		}
	},
	interceptor: {
		request: function(config, ifLoad) {
			const value = uni.getStorageSync(SET.tokenName);
			if (value) {
				config.header.Authorization = value
			}
			return config;
		}, 
		response: function(response, ifLoad) {
			let statusCode = response.statusCode
			let that = this 
			let res = response.data
			// debugger 
			// 一般在这里做全局的错误事件处理
			if (statusCode === 200) { //成功						
				if(res.result==1){
					return {status:true,data:res.data}
				}else{
					return {status:false,data:res.msg} 
				}				
			} else if (statusCode == 401) {
					uni.showToast({
						title: '登陆状态失效,请重新登陆',
						icon: 'none',
						duration: 2000
					}) 
					store.commit('userLoginOut')
					return {status:false,data:'权限失效'}  
			} else if(response.errMsg=="request:fail abort"){
				let ifLock = uni.getStorageSync('errlock')
				// console.log(ifLock)		 
				if(!ifLock){
					uni.setStorageSync('errlock',true)
				}			
				return  {status:false,data:'请检查网络'}  
			}else{				
				return  {status:false,data:response.errMsg || '错误'  }   
			}

		}
	},
	request(options, ifLoad) {
		let that = this
		if (!options) {
			options = {}
		}
		options.baseUrl = options.baseUrl || this.config.baseUrl
		options.dataType = options.dataType || this.config.dataType
		options.url = options.baseUrl + options.url
		options.data = options.data || {}
		options.method = options.method || this.config.method
		options.timeout = 10000
		// console.log('执行')
	
		return new Promise((resolve, reject) => {
			let _config = null
			
			options.complete = (response) => {
 		 
				if (this.interceptor.response) {
					// reject('没有权限')
					try{
						let re = this.interceptor.response(response, ifLoad)
						// console.log(111,re)
						if(re.status){						
							resolve(re.data);
						}else{
							throw new Error(re.data)
							
						}
					}catch(e){
						// console.log(222,e)
				 
						uni.showToast({
							icon:'none',
							title:e.toString().replace('Error:','')
						})
						reject(e);
					}
 
				}

			}

			_config = Object.assign({}, this.config, options)
			_config.requestId = new Date().getTime()
			// (_config)
			if (this.interceptor.request) {
				let re = this.interceptor.request(_config, ifLoad)
				_config = re
			}
			

			uni.request(_config);
		});
	}
}


/**
 * 请求接口日志记录
 */
function _reqlog(req) {
	if (process.env.NODE_ENV === 'development') {
		// ("【" + req.requestId + "】 地址：" + req.url)
		if (req.data) {
			// console.log("【" + req.requestId + "】 请求参数：" + JSON.stringify(req.data))
		}
	}
	//TODO 调接口异步写入日志数据库
}

/**
 * 响应接口日志记录
 */
function _reslog(res) {
	let _statusCode = res.statusCode;
	if (process.env.NODE_ENV === 'development') {
		// console.log("【" + res.config.requestId + "】 地址：" + res.config.url)
		if (res.config.data) {
			// console.log("【" + res.config.requestId + "】 请求参数：" + JSON.stringify(res.config.data))
		}
		// console.log("【" + res.config.requestId + "】 响应结果：" + JSON.stringify(res))
	}
	//TODO 除了接口服务错误外，其他日志调接口异步写入日志数据库
	switch (_statusCode) {
		case 200:
			break;
		case 401:
			break;
		case 404:
			break;
		default:
			break;
	}
}
